import { Game, IFrame } from "./Models/models";

export const start = (game: Game, numberOfFrames: number) => {
    for(let i = 0; i < numberOfFrames; i++) {
        let limit = 10;
        const frame: IFrame = {throws: {}};
        frame.throws.first = game.play(limit);
        game.subject.next(frame.throws.first);

        if (frame.throws.first === 10) {
            frame.isStrike = true;
            game.updateFrameScore(i, 2);
            game.frames = [...game.frames, frame];
            if (i === (numberOfFrames - 1)) {
                limit = 10;
                const bonus1 = game.play(limit);
                game.subject.next(bonus1);
                limit = limit - bonus1;
                const bonus2 = game.play(limit);
                game.subject.next(bonus2);
            }
            continue;
        }

        limit = 10 - frame.throws.first
        frame.throws.second = game.play(limit);
        game.subject.next(frame.throws.second);
        frame.score = frame.throws.first + frame.throws.second;

        if (frame.score === 10) {
            frame.isSpare = true;
            game.updateFrameScore(i, 1);
            game.frames = [...game.frames, frame];
            if (i === (numberOfFrames - 1)) {
                limit = 10;
                game.subject.next(game.play(limit));
            }
            continue;
        }

        game.frames = [...game.frames, frame];
    }
    return game.getTotalScore();
}

export const printFrame = (frame: IFrame, index: number) => {
    console.log('Frame: ',index + 1);
    console.log('   first throw: ', frame.throws.first);
    if (frame.throws.second) {
        console.log('   second throw: ', frame.throws.second);
    }
    if (frame.isStrike) {
        console.log('   STRIKE');
    }
    if (frame.isSpare) {
        console.log('   SPARE');
    }
    console.log('   score: ', frame.score);
    console.log('------------------------------');
}