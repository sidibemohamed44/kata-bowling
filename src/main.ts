import { Game, NUMBER_OF_FRAMES, IFrame } from "./Models/models";
import { start, printFrame } from "./play";

const game = new Game();
const totalScore = start(game, NUMBER_OF_FRAMES);
game.frames.forEach((frame: IFrame, index: number) => printFrame(frame, index));
console.log('GAME TOTAL SCORE IS: ', totalScore);