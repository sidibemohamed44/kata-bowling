import { Game, NUMBER_OF_FRAMES } from "./Models/models"
import { start } from "./play";

describe('main', () => {
    it('should initiate game of 10 frames and store score of frame', () => {
        // Given
        const game = new Game();
        // When
        start(game, NUMBER_OF_FRAMES);
        // Then
        expect(game.frames.length).toEqual(10);
    });

    it('should return game totalScore', () => {
        // Given
        const game = new Game();
        const expectedValue = 100;
        game.getTotalScore = jest.fn().mockReturnValue(expectedValue);
        // When
        const result = start(game, NUMBER_OF_FRAMES);
        // Then
        expect(result).toEqual(expectedValue);
    });

    it('should set spare score to (10 + next throw)', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 2;
        game.play = jest.fn()
            .mockReturnValueOnce(5)
            .mockReturnValueOnce(5)
            .mockReturnValueOnce(2)
            .mockReturnValueOnce(2);
        
       const expectedFramesArray = [
           {score: 12, isSpare: true, throws: {first: 5, second: 5}},
           {score: 4, throws: {first: 2, second: 2}},
       ];
       // When
       start(game, numberOfFrames);
       // Then
       expect(expectedFramesArray).toEqual(game.frames);
    });

    it(' should set strike score to (10 + 2 next throw)', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 2;
        game.play = jest.fn()
            .mockReturnValueOnce(10)
            .mockReturnValueOnce(2)
            .mockReturnValueOnce(2);
        
       const expectedFramesArray = [
           {score: 14, isStrike: true, throws: {first: 10}},
           {score: 4, throws: {first: 2, second: 2}},
       ];
       // When
       start(game, numberOfFrames);
       // Then
       expect(expectedFramesArray).toEqual(game.frames);
    });

    it(' should give 1 bonus throw when last frame is spare', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 1;
        game.play = jest.fn()
            .mockReturnValueOnce(5)
            .mockReturnValueOnce(5)
            .mockReturnValueOnce(7);
        
       const expectedFramesArray = [
           {score: 17, isSpare: true, throws: {first: 5, second: 5}},
       ];
       // When
       start(game, numberOfFrames);
       // Then
       expect(expectedFramesArray).toEqual(game.frames);
    });

    it('should give 2 bonus throw when last frame is strike', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 1;
        game.play = jest.fn()
            .mockReturnValueOnce(10)
            .mockReturnValueOnce(4)
            .mockReturnValueOnce(4);
        
       const expectedFramesArray = [
           {score: 18, isStrike: true, throws: {first: 10}},
       ];
       // When
       start(game, numberOfFrames);
       // Then
       expect(expectedFramesArray).toEqual(game.frames);
    });

    it('should return 150 when all frame score is a spare', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 10;
        game.play = jest.fn().mockReturnValue(5);        
       // When
       const result = start(game, numberOfFrames);
       // Then
       expect(result).toEqual(150);
    });

    it('should return 300 when all frame score is a strike', () => {
        // Given
        const game = new Game();
        const numberOfFrames = 10;
        game.play = jest.fn().mockReturnValue(10);        
       // When
       const result = start(game, numberOfFrames);
       // Then
       expect(result).toEqual(300);
    });
});