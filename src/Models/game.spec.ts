import { Game } from "./models";

describe('class Game', () => {
    it('should get total score of game', () => {
        // Given
        const game = new Game();
        game.frames = [{score: 10}, {score: 20}];
        // When
        const result = game.getTotalScore();
        // Then
        expect(result).toEqual(30);
    });

    it('should return random bowling pins dropped within limit of 10 in a frame', () => {
        // Given
        const game = new Game();
        let limit = 10;
        // When
        const firstThrow = game.play(limit);
        limit = 10 - firstThrow;
        const secondThrow = game.play(limit);
        // Then
        expect(firstThrow).toBeGreaterThanOrEqual(0);
        expect(firstThrow).toBeLessThanOrEqual(10);
        expect(secondThrow).toBeGreaterThanOrEqual(0);
        expect(secondThrow).toBeLessThanOrEqual(10);
    });
})