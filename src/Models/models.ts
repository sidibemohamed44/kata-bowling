export const NUMBER_OF_FRAMES = 10;
import {Subject} from 'rxjs';
import { take } from 'rxjs/operators';

interface IThrow {
	first?: number;
	second?: number;
}

export interface IFrame { 
    score?: number;
    throws?: IThrow,
    isStrike?: boolean;
    isSpare?: boolean;
}

export class Game {
    private _frames: IFrame[] = [];
    private _subject = new Subject();

    public get subject() {
        return this._subject;
    }
    public get frames() {
        return this._frames;
    }
    public set frames(frames: IFrame[]) {
        this._frames = frames;
    }
    getTotalScore() {
        return this._frames.reduce((prev, {score}) => prev + score, 0)
    }
    play(limit: number) {
        return Math.floor(Math.random() * limit + 1);
    }
    updateFrameScore(frameIndex: number, count: number) {
        this._subject
            .pipe(take(count))
            .subscribe((nextThrow: number) => {
                this._frames[frameIndex].score = this._frames[frameIndex].score ?
                    this._frames[frameIndex].score + nextThrow : 10 + nextThrow;
            });
    }
}
