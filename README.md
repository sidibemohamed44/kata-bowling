# Kata Bowling
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Ce projet permet de simuler une partie de bowling et retourne le score total du jeu.
	
## Technologies
Ce projet a été creé avec:
* Nodejs
* typescript
* RxJS Reactive progamming using Observables 

## Setup
Pour executer ce projet après l'avoir recupéré sur git, veuilez taper ces commandes npm ci-dessous:

```
$ npm install 
$ npm start
```

**NB**: le nombre de points obtenu suite à un lancer est généré de manière aleatoire.